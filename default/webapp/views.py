from django.contrib.auth.models import User
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.template import RequestContext

from recaptcha_works.decorators import fix_recaptcha_remote_ip
from webapp.forms import RecaptchaProtectedForm

def home(request):
    return render_to_response(
            'index.html',
    )

@fix_recaptcha_remote_ip
def create_user(request, *args, **kwargs):
    if request.method == 'POST':
        form = RecaptchaProtectedForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            try:
                User.objects.get(username__iexact=username)
            except User.DoesNotExist:
                user = User.objects.create_user(username=username, password=password)
                user.save()
                return HttpResponseRedirect('/')
            else:
                error = u'Username already Exist.'
                return render_to_response("create_user.html",{
                    'error':error,
                    'form':form,
                }, context_instance = RequestContext(request))
        else:
            return render_to_response("create_user.html",{
                'form': form,
            }, context_instance = RequestContext(request))
    else:
        form = RecaptchaProtectedForm()
        return render_to_response("create_user.html",{
        'form':form,
        }, context_instance = RequestContext(request))


from django import forms
from django.utils.translation import ugettext as _

from recaptcha_works.fields import RecaptchaField

class RecaptchaProtectedForm(forms.Form):
    username = forms.CharField(max_length=100, required = True)
    password = forms.CharField(widget=forms.PasswordInput(), max_length=10, required = True)
    recaptcha = RecaptchaField(required=True)

from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'webapp.views.home', name='home'),
    url(r'^register/','webapp.views.create_user', name='create_user'),
    url(r'^admin/', include(admin.site.urls)),
)
